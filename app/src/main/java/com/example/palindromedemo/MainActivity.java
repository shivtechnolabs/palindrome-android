package com.example.palindromedemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    private EditText edt_input;
    private Button btn_submit;
    private TextView txt_msg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
    }

    /*Init view */
    private void initView(){
        edt_input=(EditText)findViewById(R.id.edt_string);
        btn_submit=(Button)findViewById(R.id.btn_submit);
        txt_msg=(TextView)findViewById(R.id.txt_msg);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edt_input.getText().toString().equalsIgnoreCase("")){
                    txt_msg.setText(R.string.please_enter_input_string);
                }else {
                    isPalindrome(edt_input.getText().toString());
                }
            }
        });


    }


    /* Check String is Palindrom or not Method */
    private boolean isPalindrome(String str_input){
        // reverse string
        String str_temp= new StringBuffer(str_input).reverse().toString();

        //check input string and reverse string are same or not
        if(str_temp.equalsIgnoreCase(str_input)){
            txt_msg.setText(String.format("%s is palindrome", str_input));
            return  true;
        }else {
            txt_msg.setText(String.format("%s is Not palindrome", str_input));
            return false;
        }
    }


}
